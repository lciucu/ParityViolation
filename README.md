Report for a one year-long lab in the masters program at the University of Geneva, with partner Ana Ventura Barroso, that awarded me 15 ECTS. The .pdf file is [here](https://gitlab.cern.ch/lciucu/ParityViolation/-/blob/master/Report/Report.pdf).

It also contains a Python code to make the asymmetry plots.